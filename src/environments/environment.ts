// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBOc-IMAbZZWUfXPMWdDdEDHcnFKz6Mvt8',
    authDomain: 'task-tracker-2fa2e.firebaseapp.com',
    databaseURL: 'https://task-tracker-2fa2e.firebaseio.com',
    projectId: 'task-tracker-2fa2e',
    storageBucket: 'task-tracker-2fa2e.appspot.com',
    messagingSenderId: '89729398784',
    appId: '1:89729398784:web:6e3494da56670e0a3880e5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
